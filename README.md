# Multifunctionality analyses of the Farming System and Tillage experiment

This is a step-by-step protocol (Rcode) of the agroecosystem multifunctionality analyses of the Farming System and Tillage experiment (FAST) as presented in Wittwer et al., Organic and conservation agriculture promote ecosystem multifunctionality. Science Advances 7, eabg6995 (2021).

Published on https://raphawitt.gitlab.io/supplement_sciadv.abg6995/  

PDF version: https://raphawitt.gitlab.io/supplement_sciadv.abg6995/sciadv.abg6995.pdf 

## [License](LICENSE)

Copyright (c) 2021 Agroscope

All rights reserved.
